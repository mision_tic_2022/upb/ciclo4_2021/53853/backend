const user = process.env.USER_DB;
const pass = process.env.PASS_DB;
const database = "ecommerce_53853";

module.exports = {
    cloud_db: `mongodb+srv://${user}:${pass}@misiontic-upb.xh03m.mongodb.net/${database}`,
    local_db: `mongodb://localhost:27017/${database}`
}