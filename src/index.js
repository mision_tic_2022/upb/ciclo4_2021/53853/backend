//Configurar variables de entorno
require('dotenv').config();
//Improtar dependencias
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
//Importar modulos/clases
const IndexRouter = require('./routers/indexRouter');
const ConnDb = require('./database/connDb');
const UserRouter = require('./routers/userRouter');
const ProductRouter = require('./routers/productRouter');

class Server{
    constructor(){
        this.objConn = new ConnDb();
        this.app = express();
        this.#config();
    }


    #config(){
        //Indicar al servidor que procesará datos en formato JSON durante las peticiones http
        this.app.use(express.json());
        //Usar morgan en express para el monitoreo de las peticiones htttp
        this.app.use(morgan());
        //Permitir conexiones de origen cruzado
        this.app.use(cors());
        //Almacenar el puerto por el que correrá el servidor
        this.app.set('PORT', process.env.PORT || 3000);
        //------------CREAR RUTAS----------
        const indexR = new IndexRouter();
        const userR = new UserRouter();
        const productR = new ProductRouter();

        //--------AÑADIR RUTAS A EXPRESS---------
        this.app.use(indexR.router);
        this.app.use(userR.router);
        this.app.use(productR.router);

        //Poner el servidor a la escucha
        this.app.listen(this.app.get('PORT'), ()=>{
            console.log("Servidor corriendo por el puerto ==>> ", this.app.get('PORT'))
        });
    }


}

new Server();