const {Router} = require('express');
const ProductController = require('../controllers/productController');
const { TokenController, PRIVATE_KEY } = require('../controllers/tokenController');


class ProductRouter{
    constructor(){
        this.router = Router();
        this.#config();
    }

    #config(){        
        //Crear/configurar las rutas
        let objToken = new TokenController();   
        //Requerir autenticación para acceder a las rutas de los productos
        this.router.use(objToken.verifyAuth);     
        //Crear objeto controlador
        const objProductC = new ProductController();   
        //---------------Configuración de las rutas---------------
        this.router.post('/product', objProductC.create);
        this.router.get('/product', objProductC.get);
        this.router.put('/product', objProductC.update);
        this.router.delete('/product', objProductC.delete);
    }
}

module.exports = ProductRouter;