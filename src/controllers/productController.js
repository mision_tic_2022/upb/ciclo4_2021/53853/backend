const Product = require('../models/product');
const jwt = require('jsonwebtoken');
const { PRIVATE_KEY, TokenController } = require('./tokenController');

class ProductController{

    constructor(){
        this.objTokenC = new TokenController();
    }

    //Esto es un método
    mi_metodo(){
        console.log("Hola mundo");
    }

    //Properties Initializer    
    create = (req, res)=>{
        let {name, price} = req.body;
        let decode = jwt.decode(this.objTokenC.getToken(req), PRIVATE_KEY);
        //Insertar/crear el producto en la BD
        Product.create({name, price, user_id: decode.id}, (error, data)=>{
            if(error){
                res.status(500).json({info: error});
            }else{
                res.status(201).json(data);
            }
        });
    }

    get = (req, res)=>{
        let decode = jwt.decode(this.objTokenC.getToken(req), PRIVATE_KEY);
        Product.find({user_id: decode.id}, (error, data)=>{
            if(error){
                res.status(500).json({info: error});
            }else{
                res.status(200).json(data);
            }
        });
    }

    update = (req, res) => {
        //decodificar token
        let decode = jwt.decode(this.objTokenC.getToken(req), PRIVATE_KEY);
        let { id, name, price } = req.body;
        Product.findOneAndUpdate(
            { _id: `${id}`, userEmail: decode.email },
            { name: name, price: price },
            (err, doc) => {
                if (err) {
                    res.status(500).json({ info: err });
                } else {
                    res.status(200).json({ info: "Producto Actualizado" });
                }
            }
        );
    }

    delete = (req, res) => {
        //decodificar token
        let decode = jwt.decode(this.objTokenC.getToken(req), PRIVATE_KEY);
        let { id } = req.body;
        Product.findOneAndRemove(
            { _id: id, userEmail: decode.email },
            (err, doc) => {
                if (err) {
                    res.status(500).json({ info: err });
                } else {
                    res.status(200).json({ info: "Producto eliminado" });
                }
            }
        );
    };

}

module.exports = ProductController;